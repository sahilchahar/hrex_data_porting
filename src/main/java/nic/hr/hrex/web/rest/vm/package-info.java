/**
 * View Models used by Spring MVC REST controllers.
 */
package nic.hr.hrex.web.rest.vm;
